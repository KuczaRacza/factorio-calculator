use anyhow::Context;
use serde_json;

mod calculator;
mod crafting;
#[cfg(feature = "parser")]
mod parser;
mod tests;

#[cfg(feature = "parser")]
pub fn crate_recipe_database_json(crafting_file: &str) -> anyhow::Result<String> {
    Ok(serde_json::to_string(
        &parser::parser::load_file(crafting_file).with_context(|| "Error could not parse file")?,
    )
    .with_context(|| "Error could not write as json")?)
}
#[cfg(feature = "parser")]
pub fn crate_recipe_database_bincode(crafting_file: &str) -> anyhow::Result<Vec<u8>> {
    bincode::serialize(
        &parser::parser::load_file(crafting_file).with_context(|| "Error could not parse file")?,
    )
    .with_context(|| "Error could not write as json")
}

pub fn create_crafting_tree_from_json(database: &str) -> anyhow::Result<String> {
    serde_json::to_string(
        &calculator::calculator::calculate_with_json_db(&database)
            .with_context(|| "Error could not calculate")?,
    )
    .with_context(|| "Error could not serialize to json")
}

pub fn create_crafting_tree_from_bincode(database: &[u8]) -> anyhow::Result<String> {
    serde_json::to_string(
        &calculator::calculator::calculate_with_bincode_db(&database)
            .with_context(|| "Error could not calculate")?,
    )
    .with_context(|| "Error could not serialize to json")
}
