use crate::crafting::*;
use anyhow::{anyhow, Context, Ok};
use std::collections::HashMap;

#[derive(
    Default,
    Debug,
    serde::Serialize,
    serde::Deserialize,
    Hash,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Clone,
    Copy,
)]
struct CraftId(u32);

#[derive(Default, Debug, serde::Serialize, serde::Deserialize, Clone, PartialEq)]
struct CraftTreeItem {
    pub id: CraftId,
    pub name: String,
    pub count: f32,
}

//Craft Id represents parent of node
type CraftTree = HashMap<CraftId, Vec<CraftTreeItem>>;

#[derive(Default, Debug, serde::Serialize, serde::Deserialize, PartialEq)]
pub struct CraftingSummary {
    pub target: String,
    pub target_count: f32,
    pub tree: CraftTree,
    pub raw_resources: HashMap<String, f32>,
}

#[derive(Default, Debug, serde::Serialize, serde::Deserialize)]
struct CraftingRequest {
    pub target: String,
    pub target_count: f32,
    pub items: ItemDatabase,
    pub recipes: RecipeDatabase,
}

fn is_valid_request(request: &CraftingRequest) -> Option<anyhow::Error> {
    for recipe in &request.recipes {
        if recipe.1.time == 0 {
            return Some(anyhow::anyhow!(
                "Error recipe for item: {:?} can't have crafting time equal 0",
                recipe.0
            ));
        }
        if recipe.1.output == 0 {
            return Some(anyhow::anyhow!(
                "Error recipe for item: {:?} can't produce 0 items",
                recipe.0
            ));
        }
    }
    if request.target_count <= 0.0f32 {
        return Some(anyhow::anyhow!("Error target count must be positive value"));
    }
    None
}

fn new_craft_id(craft_id: &mut CraftId) -> CraftId {
    craft_id.0 += 1;
    craft_id.clone()
}

// TO DO  max steps
fn calculate_step(
    target: &CraftTreeItem,
    item_database: &ItemDatabase,
    recipe_database: &RecipeDatabase,
    mut last_id: CraftId,
) -> anyhow::Result<(Vec<CraftTreeItem>, CraftId)> {
    if last_id.0 > 256 {
        return Err(anyhow!("Error tree too deep"));
    }
    let target_recipe = recipe_database
        .get(&target.name)
        .with_context(|| format!("Error no recipe for {:?}", target.name))?;
    Ok((
        target_recipe
            .materials
            .iter()
            .map(|material| CraftTreeItem {
                id: new_craft_id(&mut last_id),
                count: target.count * material.count as f32 / target_recipe.output as f32,
                name: material.name.clone(),
            })
            .collect(),
        last_id,
    ))
}

fn iterate_tree(
    target: &CraftTreeItem,
    item_database: &ItemDatabase,
    recipe_database: &RecipeDatabase,
    last_id: CraftId,
) -> anyhow::Result<(CraftTree, CraftId)> {
    let mut tree = CraftTree::new();
    let (child_items, mut last_id) =
        calculate_step(target, item_database, recipe_database, last_id)
            .with_context(|| format!("Error in calculating step {:?}", target.name))?;
    for item in &child_items {
        let item_description = item_database
            .get(&item.name)
            .with_context(|| format!("Item not found in item database {:?}", item.name))?;
        if !item_description.is_raw {
            let (extension, new_last_id) =
                iterate_tree(item, item_database, recipe_database, last_id)
                    .with_context(|| format!("Error child step {:?}", item.name))?;
            tree.extend(extension);
            last_id = new_last_id;
        }
    }
    tree.insert(target.id, child_items);
    Ok((tree, last_id))
}

fn build_craft_tree(
    target: &CraftTreeItem,
    item_database: &ItemDatabase,
    recipe_database: &RecipeDatabase,
) -> anyhow::Result<CraftTree> {
    let first_item = CraftTreeItem {
        id: CraftId(1),
        count: target.count,
        name: target.name.clone(),
    };
    Ok(
        iterate_tree(&first_item, item_database, recipe_database, CraftId(1))
            .with_context(|| format!("Error cannot iterate tree"))?
            .0,
    )
}

pub fn calculate_with_json_db(request_content: &str) -> anyhow::Result<CraftingSummary> {
    let request: CraftingRequest =
        serde_json::from_str(request_content).with_context(|| "Error could not read json")?;
    if let Some(err) = is_valid_request(&request) {
        Err(err)
    } else {
        calculate(&request)
    }
}

pub fn calculate_with_bincode_db(request_content: &[u8]) -> anyhow::Result<CraftingSummary> {
    let request: CraftingRequest =
        bincode::deserialize(request_content).with_context(|| "Error could not read json")?;
    if let Some(err) = is_valid_request(&request) {
        Err(err)
    } else {
        calculate(&request)
    }
}

fn calculate(request: &CraftingRequest) -> anyhow::Result<CraftingSummary> {
    Ok(CraftingSummary {
        target: request.target.clone(),
        target_count: request.target_count,
        tree: build_craft_tree(
            &CraftTreeItem {
                id: CraftId(0),
                name: request.target.clone(),
                count: request.target_count,
            },
            &request.items,
            &request.recipes,
        )
        .with_context(|| "Error in crafting tree")?,
        ..Default::default()
    })
}

#[cfg(test)]
pub fn assert_eq_crafting(expected: &str, value: &str) {
    let expected: CraftingSummary = serde_json::from_str(expected).unwrap();
    let value: CraftingSummary = serde_json::from_str(value).unwrap();
    assert_eq!(expected, value);
}
