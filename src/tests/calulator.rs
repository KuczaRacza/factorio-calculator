use crate::calculator::calculator::{calculate_with_json_db,calculate_with_bincode_db};

#[test]
fn simple_crafting() {
    let request: String = include_str!("request_iron_gear.json").to_string();
    let summary = serde_json::to_string(&calculate_with_json_db(&request).unwrap()).unwrap();
    assert_eq!(
        summary,
        r##"{"target":"iron-gear","target_count":51.12,"tree":{"1":[{"id":2,"name":"iron-plate","count":102.24}]},"raw_resources":{}}"##
    );
    println!("{}", summary);
}
#[test]
fn multistep_crafting() {
    let request: String = include_str!("request_inserter.json").to_string();
    let summary = serde_json::to_string(&calculate_with_json_db(&request).unwrap()).unwrap();
    println!("{}", summary);
    crate::calculator::calculator::assert_eq_crafting(
        &summary,
        r##"{"target":"inserter","target_count":12.5,"tree":{"2":[{"id":5,"name":"copper-cable","count":37.5},{"id":6,"name":"iron-plate","count":12.5}],"1":[{"id":2,"name":"electronic-circuit","count":12.5},{"id":3,"name":"iron-plate","count":12.5},{"id":4,"name":"iron-gear","count":12.5}],"4":[{"id":8,"name":"iron-plate","count":25.0}],"5":[{"id":7,"name":"copper-plate","count":18.75}]},"raw_resources":{}}"##,
    );
}

#[test]
fn detect_dependency_cycle() {
    let request: String = include_str!("request_cycle.json").to_string();
    let summary = calculate_with_json_db(&request);
    assert_eq!(
        summary.err().unwrap().root_cause().to_string(),
        anyhow::anyhow!("Error tree too deep").to_string()
    )
}
