

use std::collections::HashMap;

use crate::parser::parser;
use crate::parser::parser::load_file;

#[test]
fn test_simple_recipe() {
    let lua_recipe = include_str!("simple_recipe.lua");
    parser::load_file(lua_recipe).unwrap();
}

#[test]
fn test_parse_multiple_items() {
    let items = r##"
      normal = {{"furina", 2}, "yanfei", "fischl", {"mona", 5}}
    "##
    .to_string();
    let expected = vec![
        ("furina".to_owned(), 2),
        ("yanfei".to_owned(), 1),
        ("fischl".to_owned(), 1),
        ("mona".to_owned(), 5),
    ];
    let mut lua = rlua::Lua::new();
    lua.context(|context| {
        context.load(&items).exec().unwrap();
        parser::assert_extracted_items("normal", &context.globals(), expected);
    })
}
#[test]
fn test_parse_simple_recipe() {
    let normal_expected = crate::crafting::Recipe {
        materials: vec![crate::crafting::MaterialDescription {
            name: "iron-plate".to_owned(),
            count: 2,
        }],
        output: 1,
        time: 1,
    };
    let expensive_expected = crate::crafting::Recipe {
        materials: vec![crate::crafting::MaterialDescription {
            name: "iron-plate".to_owned(),
            count: 4,
        }],
        output: 1,
        time: 1,
    };
    let parsing_out = load_file(include_str!("simple_recipe.lua")).unwrap();
    assert_eq!(*parsing_out.normal.get("iron-gear-wheel").unwrap(),normal_expected);
    assert_eq!(*parsing_out.expensive.get("iron-gear-wheel").unwrap(),expensive_expected);
}

#[test]
fn test_parse_custom_output_recipe() {
    let normal_expected = crate::crafting::Recipe {
        materials: vec![crate::crafting::MaterialDescription {
            name: "copper-plate".to_owned(),
            count: 1,
        }],
        output: 2,
        time: 1,
    };

    let parsing_out = load_file(include_str!("./multiple_output_recipe.lua")).unwrap();
    assert_eq!(*parsing_out.normal.get("copper-cable").unwrap(),normal_expected);
    assert_eq!(*parsing_out.expensive.get("copper-cable").unwrap(),normal_expected);
}

#[ignore = "No factorio files in git"]
#[test]
fn test_parse_all_factorio_base(){
    let lua_recipe = include_str!("some_recipes.lua");
    parser::load_file(lua_recipe).unwrap();
}
