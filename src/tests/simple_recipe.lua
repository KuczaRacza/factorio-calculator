data:extend({{
    type = "recipe",
    name = "iron-gear-wheel",
    normal = {
        ingredients = {{"iron-plate", 2}},
        result = "iron-gear-wheel"
    },
    expensive = {
        ingredients = {{"iron-plate", 4}},
        result = "iron-gear-wheel"
    }
}})
