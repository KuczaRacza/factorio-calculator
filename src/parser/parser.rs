use anyhow::{anyhow, Context, Ok};
use rlua::{FromLua, Table, Value};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

fn setup_context<'a>(
    lua_context: &rlua::Context<'a>,
    file_content: &str,
) -> anyhow::Result<Table<'a>> {
    let globals = lua_context.globals();
    let data_table = lua_context.create_table()?;
    globals.set("data", data_table)?;
    lua_context
        .load(include_str!("factorio-lua/dataloader.lua"))
        .exec()?;
    lua_context.load(file_content).exec()?;
    Ok(globals.get::<_, Table>("data")?)
}

#[derive(Clone, Default, Debug, Eq, PartialEq)]
struct Recipe {
    pub name: String,
    pub normal: RecipeVariant,
    pub expensive: Option<RecipeVariant>,
}

#[derive(Clone, Default, Debug, Eq, PartialEq)]
struct RecipeVariant {
    pub ingredients: Vec<(String, u32)>,
    pub result: String,
    pub result_count: u32,
    pub energy: u32,
}

type LuaRecipeDatabase = HashMap<String, Recipe>;

#[derive(Clone, Default, Debug,Serialize,Deserialize)]
pub struct ParsingOutput {
    pub normal: crate::crafting::RecipeDatabase,
    pub expensive: crate::crafting::RecipeDatabase,
}

fn parse_item(src: &rlua::Value) -> anyhow::Result<(String, u32)> {
    match src {
        Value::String(item) => Ok((item.to_str()?.to_string(), 1)),
        Value::Table(table) => Ok((
            table
                .get(1)
                .with_context(|| "Error cannot read item name")?,
            table
                .get(2)
                .with_context(|| "Error cannot read item count")?,
        )),

        _ => Err(anyhow::anyhow!("Error this is not item/item pair")),
    }
}

fn parse_items(table: &Table) -> anyhow::Result<Vec<(String, u32)>> {
    let mut items = Vec::<(String, u32)>::new();
    for it in table.clone().sequence_values() {
        items.push(
            parse_item(&it.clone().with_context(|| "Error cannot get pair")?)
                .with_context(|| format!("Error cannot parse  item index: {:?}", it))?,
        )
    }
    Ok(items)
}

fn extract_optional_items(table: &Table, key: &str) -> anyhow::Result<Option<Vec<(String, u32)>>> {
    if let Value::Table(value_table) = table
        .get::<_, Value>(key)
        .with_context(|| format!("Error cannot extract: {:?}", key))?
    {
        Ok(Some(
            parse_items(&value_table).with_context(|| "Error could parse  items in table")?,
        ))
    } else {
        Ok(None)
    }
}
fn extract_required_items(table: &Table, key: &str) -> anyhow::Result<Vec<(String, u32)>> {
    if let Value::Table(value_table) = table
        .get::<_, Value>(key)
        .with_context(|| format!("Error cannot extract: {:?}", key))?
    {
        Ok(parse_items(&value_table).with_context(|| "Error could not parse  items in table")?)
    } else {
        Err(anyhow!(format!(
            "Error field {:?} doesn't exists or is not a table",
            key
        )))
    }
}
fn extract_field_requires<'lua_time, T: FromLua<'lua_time>>(
    table: &Table<'lua_time>,
    key: &str,
) -> anyhow::Result<T> {
    table.get::<_, T>(key).with_context(|| {
        format!(
            "Error cannot extract field. Field: {:?} mismatched type or field does not exits",
            key
        )
    })
}

fn extract_field_optional<'lua_time, T: FromLua<'lua_time>>(
    table: &Table<'lua_time>,
    key: &str,
) -> anyhow::Result<Option<T>> {
    let value = table
        .get::<_, Value>(key)
        .with_context(|| format!("Error cannot extract field. Field: {:?}", key))?;
    match value {
        Value::Nil => Ok(None),
        _ => {
            //dumb but works
            Ok(Some(extract_field_requires::<T>(table, key)?))
        }
    }
}

fn parse_inner(itm_prop: &Table) -> anyhow::Result<RecipeVariant> {
    Ok(RecipeVariant {
        energy: extract_field_optional(itm_prop, "energy_required")?.unwrap_or(1),
        result_count: extract_field_optional(itm_prop, "result_count")?.unwrap_or(1),
        result: extract_field_requires(itm_prop, "result")?,
        ingredients: extract_required_items(itm_prop, "ingredients")?,
    })
}
fn parse_one_recipe(item_prop: &Table) -> anyhow::Result<Recipe> {
    if item_prop
        .contains_key("normal")
        .with_context(|| "Error cant check if field 'normal' exists")?
    {
        Ok(Recipe {
            name: extract_field_requires(item_prop, "name")?,
            normal: parse_inner(
                &extract_field_requires(item_prop, "normal")
                    .with_context(|| "Error could not extract values for normal")?,
            )?,
            expensive: match &extract_field_optional(item_prop, "expensive")? {
                None => None,
                Some(expensive_table) => Some(
                    parse_inner(expensive_table)
                        .with_context(|| "Error could not extract values for expensive")?,
                ),
            },
        })
    } else {
        Ok(Recipe {
            name: extract_field_requires(item_prop, "name")?,
            normal: parse_inner(&item_prop)
                .with_context(|| "Error could not read recipe without normal/expensive")?,
            expensive: None,
        })
    }
}

fn iterate_recipes_in_lua(data: &Table) -> anyhow::Result<LuaRecipeDatabase> {
    let recipes = data.get::<_, Table>("raw")?.get::<_, Table>("recipe")?;
    let mut database = LuaRecipeDatabase::new();
    for pair_and_error in recipes.pairs::<String, Table>() {
        let (item_name, item_props) = pair_and_error?;

        match extract_field_optional::<String>(&item_props, "category")
            .with_context(|| "Error could not check category")?
        {
            Some(_) => {
                #[cfg(test)]
                println!("Skipped {:?}, progress: {:?}", &item_name, database.len())
            }
            None => {
                let item = parse_one_recipe(&item_props)
                    .with_context(|| format!("Error could not parse recipe {:?}", item_name))?;
                database.insert(item.name.clone(), item);
                #[cfg(test)]
                println!("Parsed {:?}, progress: {:?}", &item_name, database.len())
            }
        }
    }
    Ok(database)
}

fn to_output_recipe(lua_recipe: Recipe) -> [crate::crafting::Recipe; 2] {
    [
        crate::crafting::Recipe {
            materials: lua_recipe
                .normal
                .ingredients
                .iter()
                .map(|x| x.clone().into())
                .collect(),
            time: lua_recipe.normal.energy,
            output: lua_recipe.normal.result_count,
        },
        if let Some(expensive) = lua_recipe.expensive {
            crate::crafting::Recipe {
                materials: expensive
                    .ingredients
                    .iter()
                    .map(|x| x.clone().into())
                    .collect(),
                time: expensive.energy,
                output: expensive.result_count,
            }
        } else {
            crate::crafting::Recipe {
                materials: lua_recipe
                    .normal
                    .ingredients
                    .iter()
                    .map(|x| x.clone().into())
                    .collect(),
                time: lua_recipe.normal.energy,
                output: lua_recipe.normal.result_count,
            }
        },
    ]
}

fn to_parse_output(lua_output: LuaRecipeDatabase) -> anyhow::Result<ParsingOutput> {
    let mut out = ParsingOutput::default();
    for (name, recipe) in lua_output {
        let converted = to_output_recipe(recipe);
        out.normal.insert(name.clone(), converted[0].clone());
        out.expensive.insert(name, converted[1].clone());
    }
    Ok(out)
}

pub fn load_file(file_content: &str) -> anyhow::Result<ParsingOutput> {
    let vm = rlua::Lua::new();
    let lua_output = vm
        .context(|lua_context| -> anyhow::Result<LuaRecipeDatabase> {
            let data =
                setup_context(&lua_context, file_content).with_context(|| "Cannot prepare file")?;
            iterate_recipes_in_lua(&data).with_context(|| {
                format!(
                    "Error cannot parse file: {}",
                    &file_content[0..std::cmp::min(50, (&file_content).len())]
                )
            })
        })
        .with_context(|| "Error cannot execute lua context")?;
    Ok(to_parse_output(lua_output)
        .with_context(|| "Error could not change lua to output database")?)
}

#[cfg(test)]
pub fn assert_extracted_items(key: &str, lua_table: &Table, expected: Vec<(String, u32)>) {
    let output = extract_required_items(lua_table, key).unwrap();
    assert_eq!(output, expected);
}
