use std::collections::HashMap;

#[derive(Default, Debug, serde::Serialize, serde::Deserialize)]
pub struct Item {
    pub is_raw: bool,
    pub is_liquid: bool,
}

pub type ItemDatabase = HashMap<String, Item>;

#[derive(Clone, Default, Debug, serde::Serialize, serde::Deserialize,PartialEq, Eq)]
pub struct MaterialDescription {
    pub name: String,
    pub count: u32,
}
impl From<(String, u32)> for MaterialDescription {
    fn from(value: (String, u32)) -> Self {
        Self {
            name: value.0,
            count: value.1,
        }
    }
}

#[derive(Clone, Default, Debug, serde::Serialize, serde::Deserialize,PartialEq, Eq)]
pub struct Recipe {
    pub time: u32,
    pub output: u32,
    pub materials: Vec<MaterialDescription>,
}

pub type RecipeDatabase = HashMap<String, Recipe>;
